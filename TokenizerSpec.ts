import Tokenizer, { Token, TokenizeOptions } from "./Tokenizer";

describe("Tokenizer", () => {
    xdescribe("Краткое описания данного класса", () => {
        /**
         * На вход данному классу будет поступать строка.
         * Например, в ней пользователь говорит что хочет добавить как-то продукт в дневник
         */
        const instance = new Tokenizer("Добавить на завтрак 100 грамм яблока");

        /**
         * Далее токенизируем различными сущностями.
         * То есть пользователи данного класса смогут получить знания о том, что хотел человек.
         */
        instance.tokenize({
            type: "action",
            id: 1,
            patterns: ["Добавить"],
        });

        instance.tokenize({
            type: "where",
            id: "breakfast",
            patterns: ["завтрак"],
        });

        instance.tokenizeAmount();

        instance.tokenize({
            type: "unit",
            id: "123",
            patterns: ["грамм", "гр"],
        });

        instance.tokenize({
            type: "food",
            id: "apple",
            patterns: ["яблока", "яблок"],
        });

        /**
         * Результат токенизации. Теперь мы можем понять, что хотел человек.
         */
        expect(instance.getTokens()).toEqual([
            {
                value: "Добавить",
                type: "action",
                id: 1,
            },
            {
                value: "на",
                type: "raw",
            },
            {
                value: "завтрак",
                type: "where",
                id: "breakfast",
            },
            {
                value: 100,
                type: "amount",
            },
            {
                value: "грамм",
                type: "unit",
                id: "123",
            },
            {
                value: "яблока",
                type: "food",
                id: "apple",
            },
        ]);
    });
    describe("Конструктор", () => {
        it("it should tokenize as raw", () => {
            expect(new Tokenizer("яблоко зеленое").getTokens()).toEqual([
                {
                    value: "яблоко зеленое",
                    type: "raw",
                },
            ]);
        });

        it("должен заменить дефис пробелом", () => {
            expect(new Tokenizer("сок яблочно-абрикосовый").getTokens()).toEqual([
                {
                    value: "сок яблочно абрикосовый",
                    type: "raw",
                },
            ]);
        });

        it("должен оставит дефис для слов исключений", () => {
            /**
             * Исключения:
             *  - слова типа: по-домашнему, по-мексиканский и т.п.
             *  - слова исключения: “рыба-кабан”, “кус-кус”, “чак-чак”.
             *      Реализовать в виде массива, в который возможно будут добавлены еще исключения.
             *  - слова типа: “1-152”, “3-458”, "67-70%"
             */
            expect(new Tokenizer("морковь красная по-корейски рыба-кабан по 1-152").getTokens()).toEqual([
                {
                    value: "морковь красная по-корейски рыба-кабан по 1-152",
                    type: "raw",
                },
            ]);
        });

        it("должен заменить ё на е", () => {
            expect(new Tokenizer("свёкла вареная").getTokens()).toEqual([
                {
                    value: "свекла вареная",
                    type: "raw",
                },
            ]);
        });

        it("должен привести к нижнему регистру", () => {
            expect(new Tokenizer("СвЕкЛа ВаренаЯ").getTokens()).toEqual([
                {
                    value: "свекла вареная",
                    type: "raw",
                },
            ]);
        });

        it("знаки пунктуации должны разделить на токены", () => {
            expect(new Tokenizer("свёкла вареная, подслащенная. арбуз.").getTokens()).toEqual([
                {
                    value: "свекла вареная",
                    type: "raw",
                },
                {
                    value: ",",
                    type: "punctuation",
                },
                {
                    value: "подслащенная",
                    type: "raw",
                },
                {
                    value: ".",
                    type: "punctuation",
                },
                {
                    value: "арбуз",
                    type: "raw",
                },
                {
                    value: ".",
                    type: "punctuation",
                },
            ]);
        });
    });

    describe("Tokenizer can tokenize from pattern", () => {
        /**
         * Опишем все тесты в массиве
         */
        const tasks: Array<{
            it: string; // Название теста
            inString: string; //Строка, которую будем разбивать на токены
            tokenize: TokenizeOptions[]; //Параметры, по котором будет происходить теконизация
            expectTokens: Token[]; //Результат
            skip?: boolean;
        }> = [
            {
                it: "должен токенизировать одно слово",
                inString: "яблоко зеленое 100 грамм",
                tokenize: [{ type: "food", id: 1, patterns: ["яблоко"] }],
                expectTokens: [
                    {
                        value: "яблоко",
                        type: "food",
                        id: 1,
                    },
                    {
                        value: "зеленое 100 грамм",
                        type: "raw",
                    },
                ],
            },
            {
                it: "должен токенизировать одно слово c учетом окончания существительного",
                inString: "яблоки зеленые 100 грамм",
                tokenize: [{ type: "food", id: 1, patterns: ["яблок{сщ}"] }],
                expectTokens: [
                    {
                        value: "яблоки",
                        type: "food",
                        id: 1,
                    },
                    {
                        value: "зеленые 100 грамм",
                        type: "raw",
                    },
                ],
            },
            {
                it: "должен токенизировать одно слово c учетом окончания прилагательного",
                inString: "яблоки зеленые 100 грамм",
                tokenize: [{ type: "food", id: 2, patterns: ["зелен{пр}"] }],
                expectTokens: [
                    {
                        value: "яблоки",
                        type: "raw",
                    },
                    {
                        value: "зеленые",
                        type: "food",
                        id: 2,
                    },
                    {
                        value: "100 грамм",
                        type: "raw",
                    },
                ],
            },

            {
                it: "должен токенизировать два и более слов",
                inString: "добавить яблоки зеленые 100 грамм",
                tokenize: [{ type: "food", id: 2, patterns: ["яблоки зеленые"] }],
                expectTokens: [
                    {
                        value: "добавить",
                        type: "raw",
                    },
                    {
                        value: "яблоки зеленые",
                        type: "food",
                        id: 2,
                    },
                    {
                        value: "100 грамм",
                        type: "raw",
                    },
                ],
            },

            {
                it: "должен токенизировать два и более слов с учетом окончания",
                inString: "добавить яблоки зеленые 100 грамм",
                tokenize: [{ type: "food", id: 2, patterns: ["яблок{сщ} зелен{пр}"] }],
                expectTokens: [
                    {
                        value: "добавить",
                        type: "raw",
                    },
                    {
                        value: "яблоки зеленые",
                        type: "food",
                        id: 2,
                    },
                    {
                        value: "100 грамм",
                        type: "raw",
                    },
                ],
            },

            {
                it: "должен токенизировать только с типом raw",
                inString: "яблоко красное яблоко",
                tokenize: [
                    { type: "food", id: 1, patterns: ["яблоко красное"] },
                    { type: "food", id: 2, patterns: ["яблоко"] },
                ],
                expectTokens: [
                    {
                        value: "яблоко красное",
                        type: "food",
                        id: 1,
                    },
                    {
                        value: "яблоко",
                        type: "food",
                        id: 2,
                    },
                ],
            },

            {
                it: "должен токенизировать все совпадения",
                inString: "добавить яблоки зеленые 100 грамм добавить на завтрак яблок зеленых 250 грамм",
                tokenize: [
                    { type: "action", id: 1, patterns: ["добавить"] },
                    { type: "where", id: 3, patterns: ["на завтрак"] },
                    { type: "food", id: 2, patterns: ["яблок зелен{пр}", "яблок{сщ} зелен{пр}"] },
                ],
                expectTokens: [
                    {
                        value: "добавить",
                        type: "action",
                        id: 1,
                    },
                    {
                        value: "яблоки зеленые",
                        type: "food",
                        id: 2,
                    },
                    {
                        value: "100 грамм",
                        type: "raw",
                    },
                    {
                        value: "добавить",
                        type: "action",
                        id: 1,
                    },
                    {
                        value: "на завтрак",
                        type: "where",
                        id: 3,
                    },
                    {
                        value: "яблок зеленых",
                        type: "food",
                        id: 2,
                    },
                    {
                        value: "250 грамм",
                        type: "raw",
                    },
                ],
            },
        ];

        tasks.forEach((task) => {
            if (task.skip) {
                return;
            }
            it(task.it, () => {
                const instance = new Tokenizer(task.inString);

                task.tokenize.forEach((tokenize) => {
                    instance.tokenize(tokenize);
                });

                expect(instance.getTokens()).toEqual(task.expectTokens);
            });
        });
    });

    describe("Токенизация чисел", () => {
        it("должен определить число", () => {
            expect(new Tokenizer("1500").tokenizeAmount()).toEqual([
                {
                    value: 1500,
                    type: "amount",
                },
            ]);
        });

        it("должен определить число c точкой как тысяча", () => {
            expect(new Tokenizer("1.500").tokenizeAmount()).toEqual([
                {
                    value: 1500,
                    type: "amount",
                },
            ]);
        });

        it("должен определить проценты", () => {
            expect(new Tokenizer("2,4%").tokenizeAmount()).toEqual([
                {
                    value: 2.4,
                    type: "percent",
                },
            ]);

            expect(new Tokenizer("5%").tokenizeAmount()).toEqual([
                {
                    value: 5,
                    type: "percent",
                },
            ]);

            expect(new Tokenizer("5 %").tokenizeAmount()).toEqual([
                {
                    value: 5,
                    type: "percent",
                },
            ]);
        });

        it("должен определить целое число написанные прописью", () => {
            expect(new Tokenizer("добавить яблоко сто двадцать пять грамм").tokenizeAmount()).toEqual([
                {
                    value: "добавить яблоко",
                    type: "raw",
                },
                {
                    value: 125,
                    type: "amount",
                },
                {
                    value: "грамм",
                    type: "raw",
                },
            ]);
        });

        it("должен определить полтора как число", () => {
            expect(new Tokenizer("добавить полтора яблока").tokenizeAmount()).toEqual([
                {
                    value: "добавить",
                    type: "raw",
                },
                {
                    value: 1.5,
                    type: "amount",
                },
                {
                    value: "яблока",
                    type: "raw",
                },
            ]);
        });

        it("должен определить процент написанный прописью", () => {
            expect(new Tokenizer("молоко пять процентов").tokenizeAmount()).toEqual([
                {
                    value: "молоко",
                    type: "raw",
                },
                {
                    value: 5,
                    type: "percent",
                },
            ]);

            expect(new Tokenizer("молоко 5 процентов").tokenizeAmount()).toEqual([
                {
                    value: "молоко",
                    type: "raw",
                },
                {
                    value: 5,
                    type: "percent",
                },
            ]);

            it("должен определить число с одним знаком после запятой", () => {
                expect(new Tokenizer("добавить молоко 1 и 5 процентов жирности").tokenizeAmount()).toEqual([
                    {
                        value: "добавить молоко",
                        type: "raw",
                    },
                    {
                        value: 1.5,
                        type: "percent",
                    },
                    {
                        value: "жирности",
                        type: "raw",
                    },
                ]);

                expect(new Tokenizer("добавить молоко 1 и 5% жирности").tokenizeAmount()).toEqual([
                    {
                        value: "добавить молоко",
                        type: "raw",
                    },
                    {
                        value: 1.5,
                        type: "percent",
                    },
                    {
                        value: "жирности",
                        type: "raw",
                    },
                ]);

                //так apple понимает 1 и 5.
                expect(new Tokenizer("добавить молоко 01:05 процентов жирности").tokenizeAmount()).toEqual([
                    {
                        value: "добавить молоко",
                        type: "raw",
                    },
                    {
                        value: 1.5,
                        type: "percent",
                    },
                    {
                        value: "жирности",
                        type: "raw",
                    },
                ]);

                //Но не считать это числом, если перед этим стоит слово "время"
                expect(new Tokenizer("время 01:05").tokenizeAmount()).toEqual([
                    {
                        value: "время 01:05",
                        type: "raw",
                    },
                ]);
            });
        });
    });

    describe("split", () => {
        it("должен разделить по типу", () => {
            const instance = new Tokenizer("Добавить яблоко создать продукт");

            instance.tokenize({ type: "action", id: 1, patterns: ["добавить"] });
            instance.tokenize({ type: "action", id: 2, patterns: ["создать"] });

            const instances = instance.split("action");
            expect(instances.length).toBe(2);
            expect(instances[0].getTokens()).toEqual([
                {
                    value: "добавить",
                    type: "action",
                    id: 1,
                },
                {
                    value: "яблоко",
                    type: "raw",
                },
                {
                    value: "создать",
                    type: "action",
                    id: 2,
                },
                {
                    value: "продукт",
                    type: "raw",
                },
            ]);
        });
    });
});
