type TokenType = "raw" | "amount" | "percent" | string;
type TokenId = string | number;

export interface Token {
    value: string | number;
    type: TokenType;
    id?: TokenId;
}

export interface TokenizeOptions {
    patterns: Array<string>;
    type: TokenType;
    id?: TokenId;
}

export default class Tokenizer {
    private tokens: Token[] = [];
    constructor(private string: string) {
        this.tokens.push({
            value: string,
            type: "raw",
        });
    }

    public getTokens(): Token[] {
        return this.tokens;
    }

    public tokenizeAmount(): Token[] {
        return this.getTokens();
    }
    /**
     * Функция разбивает на токены в соответствии паттернами.
     * Токенизация происходит только для токенов с type="raw"
     * Варианты паттернов:
     *  - яблоко - просто одно слово
     *  - яблоко красное - два слова
     *  - яблок{сщ} красн{пр} - все слова с окончаниями существительного и прилагательного
     *  - "сам ман мю" - поиск сразу всего слова.
     */
    public tokenize(options: TokenizeOptions): Token[] {
        //todo
        return this.getTokens();
    }

    /**
     * Функция создает несколько Tokenizer
     */
    public split(tokenType: TokenType): Tokenizer[] {
        //todo
        return null;
    }
}
